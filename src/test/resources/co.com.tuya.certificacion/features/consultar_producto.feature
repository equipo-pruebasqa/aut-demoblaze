#language: es

Característica: Consultar producto en el Portal web Demoblaze

  Yo como usuario del Portal Demoblaze
  Necesito elegir una categoria
  Para consultar productos y realizar compras

  Antecedentes:
    Dado que el usuario ingrese al portal web Demoblaze

  @ConsultaDeProductos
  Escenario: Consulta de productos
    Cuando elija una categoría y un producto
      | categoria | nombreProducto |
      | Laptops   | MacBook Pro    |
    Entonces debera visualizar los productos de esa categoria
