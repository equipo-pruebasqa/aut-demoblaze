#language: es

Característica: Inicio de sesion Portal web Demoblaze

  Yo como usuario del Portal Demoblaze
  Necesito iniciar sesion
  Para consultar productos y realizar compras

  Antecedentes:
    Dado que el usuario ingrese al portal web Demoblaze

  @InicioDeSesionExitoso
  Escenario: Inicio de sesion exitoso
    Cuando inicie sesion con sus credenciales
      | usuario  | password |
      | Analista | prueba39 |
    Entonces debera tener un ingreso exitoso