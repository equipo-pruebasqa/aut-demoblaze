package co.com.tuya.certificacion.stepdefinitions;

import co.com.tuya.certificacion.enums.Mensajes;
import co.com.tuya.certificacion.exeptions.ExcepcionGeneral;
import co.com.tuya.certificacion.models.Usuario;
import co.com.tuya.certificacion.questions.InformacionProducto;
import co.com.tuya.certificacion.questions.MensajeBienvenida;
import co.com.tuya.certificacion.tasks.AbrirPortal;
import co.com.tuya.certificacion.tasks.LogIn;
import co.com.tuya.certificacion.userinterfaces.ListaDeProductos;
import co.com.tuya.certificacion.userinterfaces.MenuDeNavegacion;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actions.Open;
import org.hamcrest.Matcher;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.util.List;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class LogInSteps {

    @Dado("que el usuario ingrese al portal web Demoblaze")
    public void queElUsuarioIngreseAlPortalWebDemoblaze() {
        theActorInTheSpotlight().wasAbleTo(AbrirPortal.demoBlaze());

    }

    @Cuando("inicie sesion con sus credenciales")
    public void inicieSesionConSusCredenciales(List<Usuario> usuario) {
        theActorInTheSpotlight().attemptsTo(LogIn.conCredenciales(usuario.get(0)));

    }

    @Entonces("debera tener un ingreso exitoso")
    public void deberaTenerUnIngresoExitoso() {
        theActorInTheSpotlight().should(seeThat(MensajeBienvenida.enElMenu()
                , is(equalTo(MenuDeNavegacion.LBL_MSJ_BIENVENIDA)))
                .orComplainWith(ExcepcionGeneral.class, "Welcome Analista"));

    }


}
