package co.com.tuya.certificacion.stepdefinitions;

import co.com.tuya.certificacion.exeptions.ExcepcionGeneral;
import co.com.tuya.certificacion.models.Producto;
import co.com.tuya.certificacion.questions.InformacionProducto;
import co.com.tuya.certificacion.tasks.SeleccionarProducto;
import co.com.tuya.certificacion.userinterfaces.ListaDeProductos;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Entonces;

import java.util.List;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class ConsultarProductoSteps {


    @Cuando("elija una categoría y un producto")
    public void elijaUnaCategoríaYUnProducto(List<Producto> producto) {
        theActorInTheSpotlight().attemptsTo(SeleccionarProducto.deLaCategoriaLaptops(producto.get(0)));


    }

    @Entonces("debera visualizar los productos de esa categoria")
    public void deberaVisualizarLosProductosDeEsaCategoria() {
        theActorInTheSpotlight().should(seeThat(InformacionProducto.enElPanel()
                , is(equalTo(ListaDeProductos.INFO_PRODUCTO)))
                .orComplainWith(ExcepcionGeneral.class, "El producto buscado no fue encontrado"));


    }



}
