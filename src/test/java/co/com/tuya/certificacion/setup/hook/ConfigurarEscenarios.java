package co.com.tuya.certificacion.setup.hook;

import static co.com.tuya.certificacion.enums.Actor.USUARIO;
import io.cucumber.java.Before;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;


public class ConfigurarEscenarios {



    @Before
    public void prepararEscenarioUsuarioDemoblaze() {
        OnStage.setTheStage(new OnlineCast());
        OnStage.theActor(USUARIO.getActor());

    }
}
