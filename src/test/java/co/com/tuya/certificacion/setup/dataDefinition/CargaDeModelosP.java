package co.com.tuya.certificacion.setup.dataDefinition;

import co.com.tuya.certificacion.models.Producto;
import co.com.tuya.certificacion.models.Usuario;
import io.cucumber.java.DataTableType;

import java.util.Map;

public class CargaDeModelosP {

    @DataTableType

    public Producto productoEntry(Map<String, String> entry) {
        return new Producto(
               entry.get("categoria"),
                entry.get("nombreProducto"));


    }
}
