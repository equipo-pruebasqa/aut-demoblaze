package co.com.tuya.certificacion.runners;


import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        glue = {"co.com.tuya.certificacion.stepdefinitions", "co.com.tuya.certificacion.setup"},
        features = "src/test/resources/co.com.tuya.certificacion/features/consultar_producto.feature",
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        dryRun = false

)


public class ConsultarProducto {
}
