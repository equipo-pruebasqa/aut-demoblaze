package co.com.tuya.certificacion.enums;

public enum  Mensajes {

    MSJ_DE_BIENVENIDA_USUARIO("Welcome Analista"),
    MSJ_ERROR_ERROR_DE_CONSULTA("No se pudo realizar la consulta del producto satisfactoriamente"),
    MSJ_ERROR_INICIO_DE_SESION("El cliente no inicio correctamente la sesion");

    private String mensajes;

    Mensajes(String mensajes) {
        this.mensajes = mensajes;
    }

    public String getMensajes() {
        return mensajes;
    }
}



