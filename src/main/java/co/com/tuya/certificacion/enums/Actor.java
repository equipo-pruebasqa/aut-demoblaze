package co.com.tuya.certificacion.enums;

public enum Actor {

    USUARIO("Analista");

    private String actor;

    Actor(String actor) {
        this.actor = actor;
    }

    public String getActor() {
        return actor;
    }
}
