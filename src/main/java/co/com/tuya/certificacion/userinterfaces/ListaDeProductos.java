package co.com.tuya.certificacion.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;

public class ListaDeProductos {

    public static final Target LBL_PRODUCTOS = Target.the("lbl productos")
            .locatedBy("*//a[text()='MacBook Pro']");

    public static final Target INFO_PRODUCTO = Target.the("Info producto")
            .locatedBy("//div[@class = 'description description-tabs']");

    private ListaDeProductos() {

    }


}
