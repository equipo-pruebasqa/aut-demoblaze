package co.com.tuya.certificacion.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class ModalAutenticacion {

    public static final Target DLG_LOG_IN = Target.the("dlg login")
            .located(By.id("logInModal"));
    public static final Target TXT_LOG_IN = Target.the("txt usuario")
            .located(By.id("loginusername"));
    public static final Target TXT_PASSWORD = Target.the("txt password")
            .located(By.id("loginpassword"));
    public static final Target BTN_LOGIN = Target.the("btn login")
            .locatedBy("(//div[@id='logInModal']//button)[3]");

    private ModalAutenticacion() {

    }
}
