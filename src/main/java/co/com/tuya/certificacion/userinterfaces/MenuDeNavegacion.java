package co.com.tuya.certificacion.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class MenuDeNavegacion {
    public static final Target LNK_LOG_IN= Target.the("lnk login")
            .located(By.id("login2"));
    public static final Target LBL_MSJ_BIENVENIDA= Target.the("lbl mensaje bienvenida")
            .located(By.id("nameofuser"));


    private MenuDeNavegacion() {

    }


}


