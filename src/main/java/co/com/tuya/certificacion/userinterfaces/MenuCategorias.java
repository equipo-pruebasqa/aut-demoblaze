package co.com.tuya.certificacion.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;

public class MenuCategorias {

    public static final Target LNK_CATEGORIA = Target.the("lnk categoria")
            .locatedBy("(//a[@class ='list-group-item'])[3]");

    private MenuCategorias() {

    }
}
