package co.com.tuya.certificacion.questions;

import co.com.tuya.certificacion.userinterfaces.ListaDeProductos;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class InformacionProducto implements Question {


    @Override
    public Object answeredBy(Actor actor) {
        return Text.of(ListaDeProductos.INFO_PRODUCTO).viewedBy(actor).asString();
    }

    public static InformacionProducto enElPanel() {
        return new InformacionProducto();
    }
}