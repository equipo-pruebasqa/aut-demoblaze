package co.com.tuya.certificacion.questions;

import co.com.tuya.certificacion.userinterfaces.MenuDeNavegacion;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class MensajeBienvenida implements Question {

    @Override
    public Object answeredBy(Actor actor) {
        return Text.of(MenuDeNavegacion.LBL_MSJ_BIENVENIDA).viewedBy(actor).asString();
    }

    public static MensajeBienvenida enElMenu() {
        return new MensajeBienvenida();
    }
}