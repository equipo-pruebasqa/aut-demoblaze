package co.com.tuya.certificacion.models;

public class Producto {

    private String categoria;
    private String nombreProducto;

    public Producto(String categoria, String nombreProducto) {
        this.categoria = categoria;
        this.nombreProducto = nombreProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
}
