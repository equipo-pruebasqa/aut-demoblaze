package co.com.tuya.certificacion.tasks;

import co.com.tuya.certificacion.userinterfaces.PortalDemoblazeUrl;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class AbrirPortal implements Task {


    @Override
    public <T extends Actor> void performAs(T actor) {
       actor.wasAbleTo(Open.browserOn().the(PortalDemoblazeUrl.class));

    }

    public static AbrirPortal demoBlaze(){
        return Tasks.instrumented(AbrirPortal.class);
    }
}
