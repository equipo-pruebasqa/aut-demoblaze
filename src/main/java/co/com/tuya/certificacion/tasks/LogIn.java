package co.com.tuya.certificacion.tasks;

import co.com.tuya.certificacion.models.Usuario;
import co.com.tuya.certificacion.userinterfaces.MenuDeNavegacion;
import co.com.tuya.certificacion.userinterfaces.ModalAutenticacion;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.tuya.certificacion.userinterfaces.MenuDeNavegacion.LNK_LOG_IN;
import static co.com.tuya.certificacion.userinterfaces.ModalAutenticacion.*;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isNotVisible;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class LogIn implements Task {

    private Usuario usuario;

    public LogIn(Usuario usuario) {
        this.usuario = usuario;
    }

        @Override
        public <T extends Actor > void performAs (T actor){
            actor.attemptsTo(
                    Click.on(LNK_LOG_IN),
                    WaitUntil.the(DLG_LOG_IN, isVisible()),
                    Enter.theValue(usuario.getUsuario()).into(TXT_LOG_IN),
                    Enter.theValue(usuario.getPassword()).into(TXT_PASSWORD),
                    Click.on(BTN_LOGIN),
                    WaitUntil.the(DLG_LOG_IN, isNotVisible()));






        }




    public static LogIn conCredenciales(Usuario usuario){
            return Tasks.instrumented(LogIn.class, usuario);
        }

}