package co.com.tuya.certificacion.tasks;

import co.com.tuya.certificacion.models.Producto;
import co.com.tuya.certificacion.userinterfaces.ListaDeProductos;
import co.com.tuya.certificacion.userinterfaces.MenuCategorias;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.tuya.certificacion.userinterfaces.ListaDeProductos.LBL_PRODUCTOS;
import static co.com.tuya.certificacion.userinterfaces.MenuCategorias.LNK_CATEGORIA;

public class SeleccionarProducto implements Task {

    private Producto producto;

    public SeleccionarProducto(Producto producto) {
        this.producto = producto;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Scroll.to(LNK_CATEGORIA),
                Click.on(LNK_CATEGORIA),
                Scroll.to(LBL_PRODUCTOS),
                Click.on(LBL_PRODUCTOS)

                );

    }


    public static SeleccionarProducto deLaCategoriaLaptops(Producto producto){
        return Tasks.instrumented(SeleccionarProducto.class, producto);

    }
}
