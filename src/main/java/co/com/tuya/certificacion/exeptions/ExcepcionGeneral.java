package co.com.tuya.certificacion.exeptions;

public class ExcepcionGeneral extends AssertionError{

    public static final long serialVersionUID = 1;

    public ExcepcionGeneral(String message, Throwable cause) {
        super(message, cause);
    }


}
